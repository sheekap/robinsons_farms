# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
RobinsonFarms::Application.config.secret_key_base = 'bdfd970309818e7d4150d82168904afdb59d33817b02c3628480fbd91e01b15f4c78effe94e7f6d6119b06dc813795745fe2409093cced52f200f45865e9e097'
