require 'test_helper'

class GeneralPagesControllerTest < ActionController::TestCase
  setup do
    @general_page = general_pages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:general_pages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create general_page" do
    assert_difference('GeneralPage.count') do
      post :create, general_page: {  }
    end

    assert_redirected_to general_page_path(assigns(:general_page))
  end

  test "should show general_page" do
    get :show, id: @general_page
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @general_page
    assert_response :success
  end

  test "should update general_page" do
    patch :update, id: @general_page, general_page: {  }
    assert_redirected_to general_page_path(assigns(:general_page))
  end

  test "should destroy general_page" do
    assert_difference('GeneralPage.count', -1) do
      delete :destroy, id: @general_page
    end

    assert_redirected_to general_pages_path
  end
end
