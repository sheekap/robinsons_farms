class GeneralPagesController < ApplicationController
  before_action :set_general_page, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:show, :home]

  # GET /general_pages
  # GET /general_pages.json
  def index
    @general_pages = GeneralPage.all
  end

  def admin    
  end

  def home
    @general_page = GeneralPage.first 
    @about = Category.where(:name => 'About Us').first
    @product = Category.where(:name => 'Our Products').first
    @haylage = Subcategory.where(:name => 'Haylage').first
    @haylage_products = ListItem.where(:subcategory_id => @haylage.id).first(3)
    @equipment = Subcategory.where(:name => 'Equipment').first
    @client = Subcategory.where(:name => 'Our Clients').first
  end

  # GET /general_pages/1
  # GET /general_pages/1.json
  def show
  end

  # GET /general_pages/new
  def new
    @general_page = GeneralPage.new
  end

  # GET /general_pages/1/edit
  def edit
  end

  # POST /general_pages
  # POST /general_pages.json
  def create
    @general_page = GeneralPage.new(general_page_params)

    respond_to do |format|
      if @general_page.save
        format.html { redirect_to root_url, notice: 'General page was successfully created.' }
        format.json { render action: 'show', status: :created, location: @general_page }
      else
        format.html { render action: 'new' }
        format.json { render json: @general_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /general_pages/1
  # PATCH/PUT /general_pages/1.json
  def update
    respond_to do |format|
      if @general_page.update(general_page_params)
        format.html { redirect_to root_url, notice: 'General page was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @general_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /general_pages/1
  # DELETE /general_pages/1.json
  def destroy
    @general_page.destroy
    respond_to do |format|
      format.html { redirect_to general_pages_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_general_page
      @general_page = GeneralPage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def general_page_params
      params.require(:general_page).permit(:name, :home_page_banner, :menu_image, :phone_image, :phone_number, 
        :home_page_title, :home_page_description, :home_mobile_banner, :products_home_page_top_image, 
        :haylage_home_banner, :haylage_inset_image, :equipment_home_banner, :equipment_inset_image, :clients_logo_images,
        :client_image_left, :client_image_middle, :client_image_right, :haylage_content, :haylage_header, :equipment_content,
        :quote, :haylage_link_one, :haylage_link_two, :equipment_link, :clients_link, :clients_divider_image,
        :subcategory_divider_image)
    end
end
