json.array!(@general_pages) do |general_page|
  json.extract! general_page, :id
  json.url general_page_url(general_page, format: :json)
end
