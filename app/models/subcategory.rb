class Subcategory < ActiveRecord::Base
	belongs_to :category
	has_many :list_items

	mount_uploader :main_image, ImageUploader
	mount_uploader :banner_image, ImageUploader
	mount_uploader :content_image, ImageUploader
	mount_uploader :bottom_divider_image, ImageUploader
end
