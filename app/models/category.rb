class Category < ActiveRecord::Base
	has_many :subcategories
	
	mount_uploader :main_image, ImageUploader
	mount_uploader :banner_image, ImageUploader
end
