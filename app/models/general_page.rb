class GeneralPage < ActiveRecord::Base
	mount_uploader :home_page_banner, ImageUploader
	mount_uploader :home_mobile_banner, ImageUploader
	mount_uploader :menu_image, ImageUploader
	mount_uploader :phone_image, ImageUploader
	mount_uploader :products_home_page_top_image, ImageUploader
	mount_uploader :haylage_home_banner, ImageUploader
	mount_uploader :haylage_inset_image, ImageUploader
	mount_uploader :equipment_home_banner, ImageUploader
	mount_uploader :equipment_inset_image, ImageUploader
	mount_uploader :clients_logo_images, ImageUploader
	mount_uploader :client_image_left, ImageUploader
	mount_uploader :client_image_middle, ImageUploader
	mount_uploader :client_image_right, ImageUploader
	mount_uploader :clients_divider_image, ImageUploader
end
