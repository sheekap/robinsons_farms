class ListItem < ActiveRecord::Base
	belongs_to :subcategory

	mount_uploader :item_image, ImageUploader
	mount_uploader :home_page_image, ImageUploader
end
