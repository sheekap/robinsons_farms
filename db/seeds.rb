Category.delete_all
GeneralPage.delete_all
Subcategory.delete_all

gp = GeneralPage.create(
  home_page_banner: "home1.jpg",
  menu_image: "assets/menu01.png",
  phone_image: "assets/RF_phone.png",
  phone_number: "(01) 628 6554",
  home_page_title: "Top Nutrition For Your Horse",
  home_page_description: "<p>The Robinson family have been producing top quality haylage for over 30 years for champion horses, leisure horses and ponies.</p>",
  home_mobile_banner: "assets/home_mob.jpg",
  products_home_page_top_image: "assets/products02.jpg",
  haylage_home_banner: "assets/RF_web_home_4.jpg",
  haylage_inset_image: "assets/product-image-02.jpg",
  equipment_home_banner: "assets/equipment_banner.jpg",
  equipment_inset_image: nil,
  clients_logo_images: "assets/clientsmerged_01.png",
  client_image_left: "assets/clientsmerged_02.png",
  client_image_middle: "assets/clientsmerged_04.png",
  client_image_right: "assets/clientsmerged_03.png",
  haylage_content: "<p>- A healthy respiratory system<br />- Consistent quality<br />- High nutritional value thus allowing hard feed quantity to be reduced</p>",
  haylage_header: "Our haylage provides your horse with:",
  equipment_content: "<h4>A revolution in hay making...</h4><p>We are the exclusive suppliers of Veda Hay Dryers across the UK and Ireland</p><p>Finding the right machine for your needs is important to us. We will guide and advise you in your decision</p>",
  quote: "Few things are as important to the performance of my horse as clean lungs and a healthy respiratory system. That's why I feed haylage from Robinson Farms. It's dust free, nutritious and helps to keep condition on working horses. Quality made easy.",
  haylage_link_one: "Buy Now",
  haylage_link_two: "How To Feed?",
  equipment_link: "Discover More",
  clients_link: "Discover More",
  clients_divider_image: "RF_web_home_7.jpg")


c1 = Category.create(
  name: "About Us",
  page_title: "12 Years of Tradition",
  page_description: "<p>Robinson Farms is Irelands leading hay and haylage supplier. It is no coincidence that we are based in Co. Kildare, Ireland.&nbsp;</p><p>Our top quality horse feed is grown on Kildare's limestone land - one of the most fertile soils in the world.&nbsp;</p>",
  page_content: "<p>Here we produce high quality dust free forage to many clients from leading stud farms , racing and competition yards to even horse and pony owners.</p>\r\n<p>We guarentee that our hay and haylage has no blending and is dust free.</p>\r\n<p>We regularly test feeds for microtoxins and to ensure the highest quality for our customers.&nbsp;</p>",
  link_to_subcategory: "Meet the Robinson's",
  banner_image: "RF_web_about_us_1.jpg",
  main_image: "RF_web_about_us_2.jpg",
  home_page_content: "<h4>The Robinsons</h4><p>The Robinsons family business have over 100 years farming experience. They know the nutrition horses need and are the best at providing it, say top horse trainers, horse owners, pony owners and stud farms.&nbsp;</p>"
  )

c2 = Category.create(
  name: "Our Products",
  page_title: "Food of champions",
  page_description: "<p>The Robinsons guarantee a clean feed and regularly test their haylage to ensure no microtoxins or dust.&nbsp;</p>",
  page_content: "<h3>Our haylage provides your horse with:</h3><p>- A healthy respiratory system<br />- Consistent quality<br />- High nutritional value thus allowing hard feed quantity to be reduced</p><h2>&nbsp;</h2>",
  link_to_subcategory: "Discover More",
  banner_image: "products_header_image.png",
  main_image: "products_page_01.png"
)

meet = Subcategory.create(
  name: "Meet The Robinsons",
  category_id: c1.id,
  page_title: "Meet the Robinson's",
  page_description: "<h3>The Heritage</h3><p>We pride ourselves in producing top nutrition for the horse industry</p>",
  content_title: "Robinson Farms is a family run business now in it's fourth generation",
  content_description: "<h3>We stand for...</h3><p>. Quality . Equine Health . Consistency . Delivery . Customer Service . Reliability</p>",
  optional_information: "",
  main_image: "RF_web_meet_2.jpg",
  banner_image: "RF_web_meet_1.jpg"
  )

haylage = Subcategory.create(
  name: "Haylage",
  category_id: c2.id,
  page_title: "Food of Champions",
  page_description: "<p>The Robinson family have been producing top quality haylage for over 30 years for champion horses, leisure horses and ponies.&nbsp;</p>",
  content_title: "Haylage",
  main_image: "products_page_01.png",
  banner_image: "products_header_image.png",
  home_page_content: "<h3>Our haylage provides your horse with:</h3><p>- A healthy respiratory system<br />- Consistent quality<br />- High nutritional value so hard feed quantity can be reduced</p>",
  link_one: "Buy Now", 
  link_two: "How To Feed?",
  banner_link_text: "Discover More",
  bottom_divider_image: "RF_web_home_7.jpg"
  )

equipment = Subcategory.create(
  name: "Equipment",
  category_id: c2.id, 
  page_title: "A Revolution in making hay", 
  page_description: "<p>We are the exclusive supplier and distributor of Veda hay dryers across the UK and Ireland.&nbsp;</p>", 
  content_title: "Hay Dryers", 
  content_description: "", 
  optional_information: "", 
  main_image: "RF_web_home_7.jpg", 
  banner_image: "equipment_landing_banner.jpg", 
  home_page_content: "<p>A revolution in hay making...</p><p>We are the exclusive providers of Veda Hay Dryers across the UK and Ireland.</p><p>Finding the right machine for your needs is important to us. We will guide and advise you in your decision.&nbsp;</p>", 
  link_one: "Discover More", 
  link_two: "", 
  banner_link_text: "", 
  bottom_divider_image: nil
  )

clients = Subcategory.create(
  name: "Our Clients", 
  category_id: c1.id, 
  page_title: "Our Clients", 
  page_description: "<p>11 Years ago Robinson Farms expanded its operations and began producing haylage. Being the first Irish supplier of equine dust free haylage that is conveniently packed means that we have attracted many long standing customers.</p>", 
  main_image: "clients13.jpg", 
  banner_image: "RF_web_meet_2.jpg", 
  content_image: nil, 
  home_page_content: "", 
  link_one: "", 
  link_two: "", 
  banner_link_text: "Discover More", 
  bottom_divider_image: nil
  )

ListItem.create!([
  {name: "Italian Rhye", description: "<p>Round Bale Size: *4x4 (1.20x1.20)</p>\r\n<p>Big Square Bale Size: *3x4 (0.90x1.20)</p>\r\n<p>Energy Available: *Three Phase *Single Phase *Natural Gas *Propane</p>\r\n<p>Dieseland advise you in your decision</p>", item_image: "product-image-02.jpg", optional_information: "", link_text: "Contact Us", subcategory_id: haylage.id, home_page_image: "products03.jpg"},
  {name: "Timothy", description: "<p>Round Bale Size: *4x4 (1.20x1.20)</p>\r\n<p>Big Square Bale Size: *3x4 (0.90x1.20)</p>\r\n<p>Energy Available: *Three Phase *Single Phase *Natural Gas *Propane</p>\r\n<p>Dieseland advise you in your decision</p>", item_image: "product-image-02.jpg", optional_information: "", link_text: "Contact Us", subcategory_id: haylage.id, home_page_image: "products03.jpg"},
  {name: "Mixed Meadow", description: "<p>Round Bale Size: *4x4 (1.20x1.20)</p>\r\n<p>Big Square Bale Size: *3x4 (0.90x1.20)</p>\r\n<p>Energy Available: *Three Phase *Single Phase *Natural Gas *Propane</p>\r\n<p>Dieseland advise you in your decision</p>", item_image: "product-image-02.jpg", optional_information: "", link_text: "Contact Us", subcategory_id: haylage.id, home_page_image: "products03.jpg"},
  {name: "Hay Dryer Mod.TO1 - One Module, 30 HP 1,000,000 BTU", description: "<p>Round Bale Size: *4x4 (1.20x1.20)</p>\r\n<p>Big Square Bale Size: *3x4 (0.90x1.20)</p>\r\n<p>Energy Available: *Three Phase *Single Phase *Natural Gas *Propane</p>\r\n<p>Dieseland advise you in your decision</p>", item_image: "product-image-01.jpg", optional_information: "", link_text: "Contact Us", subcategory_id: equipment.id, home_page_image: nil},
  {name: "Hay Dryer Mod.TO2 - Two Modules, 60 HP 2,000,000 BTU", description: "<p>Round Bale Size: *4x4 (1.20x1.20)</p>\r\n<p>Big Square Bale Size: *3x4 (0.90x1.20)</p>\r\n<p>Energy Available: *Three Phase *Single Phase *Natural Gas *Propane</p>\r\n<p>Dieseland advise you in your decision</p>", item_image: "product-image-01.jpg", optional_information: "", link_text: "Contact Us", subcategory_id: equipment.id, home_page_image: nil},
  {name: "Hay Dryer Mod.TO3 - Three Modules, 75 HP 3,000,000 BTU", description: "<p>Round Bale Size: *4x4 (1.20x1.20)</p>\r\n<p>Big Square Bale Size: *3x4 (0.90x1.20)</p>\r\n<p>Energy Available: *Three Phase *Single Phase *Natural Gas *Propane</p>\r\n<p>Dieseland advise you in your decision</p>", item_image: "product-image-01.jpg", optional_information: "", link_text: "Contact Us", subcategory_id: equipment.id, home_page_image: nil},
  {name: "Peter Robinson", description: "<p>My passion is to grow crops and save them successfully.&nbsp;</p>", item_image: "RF_web_meet_3.jpg", optional_information: "<p>Peter is the 3rd generation of Robinson Farms. Growing up in the farm was a big adventure for him.&nbsp;<br />He decided to take it seriously and completed Agriculture at Warrenstown College. Peter is a pioneer in the drying of haylage. He has over 45 years experience</p>", link_text: "", subcategory_id: meet.id, home_page_image: nil},
  {name: "David Robinson", description: "<p>I love farming, the machinery and producing a high quality product.&nbsp;</p>", item_image: "RF_web_meet_4.jpg", optional_information: "<p>David is Peter's son. He is the 4th generation of Robinson Farms. He started off as a carpenter but quickly realised that farming was where he belonged.&nbsp;<br />He completed his Agriculture studies at Gortain College and has been working on the farm ever since.&nbsp;</p>", link_text: "Discover our Products", subcategory_id: meet.id, home_page_image: nil}
])
