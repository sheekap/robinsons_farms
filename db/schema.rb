# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140622154601) do

  create_table "categories", force: true do |t|
    t.string   "name"
    t.string   "page_title"
    t.text     "page_description"
    t.text     "page_content"
    t.string   "link_to_subcategory"
    t.string   "banner_image"
    t.string   "main_image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "home_page_content"
  end

  create_table "general_pages", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "home_page_banner"
    t.string   "menu_image"
    t.string   "phone_image"
    t.string   "phone_number"
    t.string   "home_page_title"
    t.text     "home_page_description"
    t.string   "home_mobile_banner"
    t.string   "products_home_page_top_image"
    t.string   "haylage_home_banner"
    t.string   "haylage_inset_image"
    t.string   "equipment_home_banner"
    t.string   "equipment_inset_image"
    t.string   "clients_logo_images"
    t.string   "client_image_left"
    t.string   "client_image_middle"
    t.string   "client_image_right"
    t.text     "haylage_content"
    t.string   "haylage_header"
    t.text     "equipment_content"
    t.text     "quote"
    t.string   "haylage_link_one"
    t.string   "haylage_link_two"
    t.string   "equipment_link"
    t.string   "clients_link"
    t.string   "clients_divider_image"
  end

  create_table "list_items", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "item_image"
    t.text     "optional_information"
    t.string   "link_text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "subcategory_id"
    t.string   "home_page_image"
  end

  create_table "subcategories", force: true do |t|
    t.string   "name"
    t.integer  "category_id"
    t.string   "page_title"
    t.text     "page_description"
    t.string   "content_title"
    t.text     "content_description"
    t.text     "optional_information"
    t.string   "main_image"
    t.string   "banner_image"
    t.string   "content_image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "home_page_content"
    t.string   "link_one"
    t.string   "link_two"
    t.string   "banner_link_text"
    t.string   "bottom_divider_image"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
