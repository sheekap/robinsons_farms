class CreateListItems < ActiveRecord::Migration
  def change
    create_table :list_items do |t|
    	t.string :name
    	t.text :description
    	t.string :item_image
    	t.text :optional_information
    	t.string :link_text

      t.timestamps
    end
  end
end
