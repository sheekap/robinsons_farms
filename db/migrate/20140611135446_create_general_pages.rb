class CreateGeneralPages < ActiveRecord::Migration
  def change
    create_table :general_pages do |t|
    	t.string :name

      t.timestamps
    end
  end
end
