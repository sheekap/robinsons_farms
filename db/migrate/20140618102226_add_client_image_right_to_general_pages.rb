class AddClientImageRightToGeneralPages < ActiveRecord::Migration
  def change
    add_column :general_pages, :client_image_right, :string
  end
end
