class AddHomeMobileBannerToGeneralPages < ActiveRecord::Migration
  def change
    add_column :general_pages, :home_mobile_banner, :string
  end
end
