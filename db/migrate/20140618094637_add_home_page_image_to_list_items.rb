class AddHomePageImageToListItems < ActiveRecord::Migration
  def change
    add_column :list_items, :home_page_image, :string
  end
end
