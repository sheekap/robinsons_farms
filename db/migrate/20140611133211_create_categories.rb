class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
    	t.string :name
    	t.string :page_title
    	t.text :page_description
    	t.text :page_content
    	t.string :link_to_subcategory
    	t.string :banner_image
    	t.string :main_image

      t.timestamps
    end
  end
end
