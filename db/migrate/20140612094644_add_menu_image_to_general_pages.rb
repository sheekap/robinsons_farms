class AddMenuImageToGeneralPages < ActiveRecord::Migration
  def change
    add_column :general_pages, :menu_image, :string
  end
end
