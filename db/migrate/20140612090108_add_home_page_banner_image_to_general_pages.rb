class AddHomePageBannerImageToGeneralPages < ActiveRecord::Migration
  def change
    add_column :general_pages, :home_page_banner, :string
  end
end
