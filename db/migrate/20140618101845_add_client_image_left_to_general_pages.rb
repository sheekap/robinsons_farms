class AddClientImageLeftToGeneralPages < ActiveRecord::Migration
  def change
    add_column :general_pages, :client_image_left, :string
  end
end
