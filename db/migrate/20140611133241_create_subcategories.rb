class CreateSubcategories < ActiveRecord::Migration
  def change
    create_table :subcategories do |t|
    	t.string :name
    	t.integer :category_id
    	t.string :page_title
    	t.text :page_description
    	t.string :content_title
    	t.text :content_description
    	t.text :optional_information
    	t.string :main_image
    	t.string :banner_image
    	t.string :content_image

      t.timestamps
    end
  end
end
