class AddEquipmentContentToGeneralPages < ActiveRecord::Migration
  def change
    add_column :general_pages, :equipment_content, :text
  end
end
