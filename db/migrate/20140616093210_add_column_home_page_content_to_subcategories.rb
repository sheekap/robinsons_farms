class AddColumnHomePageContentToSubcategories < ActiveRecord::Migration
  def change
    add_column :subcategories, :home_page_content, :text
  end
end
