class AddQuoteToGeneralPages < ActiveRecord::Migration
  def change
    add_column :general_pages, :quote, :text
  end
end
