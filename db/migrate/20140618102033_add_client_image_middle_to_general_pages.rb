class AddClientImageMiddleToGeneralPages < ActiveRecord::Migration
  def change
    add_column :general_pages, :client_image_middle, :string
  end
end
