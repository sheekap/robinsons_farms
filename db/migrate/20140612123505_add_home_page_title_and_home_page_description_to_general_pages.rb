class AddHomePageTitleAndHomePageDescriptionToGeneralPages < ActiveRecord::Migration
  def change
    add_column :general_pages, :home_page_title, :string
    add_column :general_pages, :home_page_description, :text
  end
end
