class AddClientsDividerImageToGeneralPages < ActiveRecord::Migration
  def change
    add_column :general_pages, :clients_divider_image, :string
  end
end
