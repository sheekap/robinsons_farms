class AddProductsHomePageTopImageToGeneralPages < ActiveRecord::Migration
  def change
    add_column :general_pages, :products_home_page_top_image, :string
  end
end
