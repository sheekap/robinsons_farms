class AddColumnHomePageContentToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :home_page_content, :text
  end
end
