class AddLinksToSubcategories < ActiveRecord::Migration
  def change
    add_column :subcategories, :link_one, :string
    add_column :subcategories, :link_two, :string
  end
end
