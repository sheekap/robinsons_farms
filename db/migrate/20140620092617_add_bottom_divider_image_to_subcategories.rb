class AddBottomDividerImageToSubcategories < ActiveRecord::Migration
  def change
    add_column :subcategories, :bottom_divider_image, :string
  end
end
