class AddLinkColumnsToGeneralPages < ActiveRecord::Migration
  def change
    add_column :general_pages, :haylage_link_one, :string
    add_column :general_pages, :haylage_link_two, :string
    add_column :general_pages, :equipment_link, :string
    add_column :general_pages, :clients_link, :string
  end
end
