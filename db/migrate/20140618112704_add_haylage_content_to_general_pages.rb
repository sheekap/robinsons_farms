class AddHaylageContentToGeneralPages < ActiveRecord::Migration
  def change
    add_column :general_pages, :haylage_content, :text
  end
end
