class AddPhoneImageAndNumberToGeneralPages < ActiveRecord::Migration
  def change
    add_column :general_pages, :phone_image, :string
    add_column :general_pages, :phone_number, :string
  end
end
