class AddBannerLinkTextToSubcategories < ActiveRecord::Migration
  def change
    add_column :subcategories, :banner_link_text, :string
  end
end
