class AddSubcategoryIdToListItems < ActiveRecord::Migration
  def change
    add_column :list_items, :subcategory_id, :integer
  end
end
