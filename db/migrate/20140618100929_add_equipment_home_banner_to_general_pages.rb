class AddEquipmentHomeBannerToGeneralPages < ActiveRecord::Migration
  def change
    add_column :general_pages, :equipment_home_banner, :string
  end
end
