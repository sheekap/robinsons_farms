class AddClientsLogoImagesToGeneralPages < ActiveRecord::Migration
  def change
    add_column :general_pages, :clients_logo_images, :string
  end
end
